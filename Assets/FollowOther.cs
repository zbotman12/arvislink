﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//[ExecuteInEditMode]
public class FollowOther : MonoBehaviour {

	public Transform A, B;
	BezierCurve Curve;
	BezierPoint[] anchors;
	public float curveValue;

	void Start()
	{
		Curve = GetComponent<BezierCurve>();
		anchors = Curve.GetAnchorPoints();
	}


	// Update is called once per frame
	void Update () {
		Vector3 cross = (Vector3.Cross(-A.transform.forward, -B.transform.forward).y <= 0) ?
							new Vector3(0,Vector3.Cross(-A.transform.forward, -B.transform.forward).y + 1,0) :
							new Vector3(0,0,0);
		// if(cross.y > 1)
		// {
		// 	cross -= new Vector3(0,1,0);
		// }
		Debug.Log((Vector3.Cross(-A.transform.forward, -B.transform.forward).y + 1) / 2.0f);
		Vector3 z = cross * Vector3.Distance(A.transform.localPosition,Camera.main.transform.position)/10;
		float handleMultiplier = Mathf.Min(curveValue, Mathf.Abs((Vector3.Distance(B.transform.localPosition, A.transform.localPosition) -1.1f) *10));
		anchors[0].position = A.transform.position;
		anchors[0].handle1 = (A.transform.localPosition - A.transform.forward * -handleMultiplier) + z * curveValue;
		anchors[0].handle2 = (A.transform.localPosition - A.transform.forward * handleMultiplier) + z  *curveValue;
	
		//anchors[2].position = B.transform.position - B.transform.forward * 3.0f;
		anchors[1].position = B.transform.position;
		anchors[1].handle1 = (B.transform.localPosition - B.transform.forward * handleMultiplier) - z * curveValue;
		anchors[1].handle2 = (B.transform.localPosition - B.transform.forward * -handleMultiplier) - z * curveValue;	
	}
}
