﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Pipe : MonoBehaviour {

	[HideInInspector]
	public Transform pointA, pointB;
	public int lineSteps = 20;
	public float Amplitude = 0.3f;
	public Vector3 normal = Vector3.up;
	LineRenderer lr;

	void Awake()
	{
		if(pointA != null && pointB != null)
			MakeLineConnection(pointA.position, pointB.position);
	}

	void Start()
	{
		lr = GetComponent<LineRenderer>();
	}

	void Update()
	{
		//TODO: Only have to call this when a target transform updates
		if(pointA != null && pointB != null)
			MakeLineConnection(pointA.position, pointB.position);
	}

	void MakeLineConnection(Vector3 a, Vector3 b)
	{
		lr.positionCount = lineSteps + 1;
		Vector3[] points = new Vector3[lineSteps + 1];
		for(int i = 0; i < lineSteps; i++)
		{
			points[i] = Vector3.Lerp(a,b,(float)i/(float)lineSteps) + 
			normal * Amplitude * Mathf.Sin((float)i/(float)lineSteps * 180.0f * Mathf.Deg2Rad);
		}
		points[lineSteps] = b;
		lr.SetPositions(points);
	}

	public void SetTargets(Transform a, Transform b)
	{
		pointA = a;
		pointB = b;
	}

	public bool CompareTargets(Transform a, Transform b)
	{
		if ((pointA == a || pointA == b) && (pointB == a || pointB == b))
		{
			return true;
		}else
		{
			return false;
		}
	}
}
